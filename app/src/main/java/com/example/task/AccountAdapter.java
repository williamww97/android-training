package com.example.task;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.model.Account;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class AccountAdapter extends RecyclerView.Adapter<AccountViewHolder> implements Filterable  {

    public interface CallbackAdapter{
        void onClick(Account acc);
    }

    public CallbackAdapter callbackAdapter;

    Context c;
    ArrayList<Account> accounts;
    ArrayList<Account> accountsFilter;

    public void setDataAdapter(ArrayList<Account> laccount){
        this.accounts = laccount;
    }

    public AccountAdapter(Context c, CallbackAdapter callbackAdapter) {

        this.c = c;
        //this.accounts = accounts;
        //this.accountsFilter = new ArrayList();
        //accountsFilter.addAll(accounts);
        this.callbackAdapter = callbackAdapter;
    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_body, parent, false);

        return new AccountViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull AccountViewHolder holder, int position) {
        holder.tv_acc.setText(accounts.get(position).getAccount());
        holder.tv_name.setText(accounts.get(position).getName());
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackAdapter.onClick(accounts.get(position));
            }
        });
    }



    @Override
    public int getItemCount() {
        return accounts.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Account> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(accountsFilter);
            } else {
                for(int i = 0 ; i < accountsFilter.size();i++){
                    if(accountsFilter.get(i).getAccount().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(accountsFilter.get(i));
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            accountsFilter.clear();
            accountsFilter.addAll((Collection<? extends Account>) results.values);
            notifyDataSetChanged();
        }
    };
}
