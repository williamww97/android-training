package com.example.task;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AccountViewHolder extends RecyclerView.ViewHolder {
    TextView tv_acc;
    TextView tv_name;


    public AccountViewHolder(@NonNull View itemView) {
        super(itemView);

        this.tv_acc = itemView.findViewById(R.id.t_account);
        this.tv_name = itemView.findViewById(R.id.t_name);
    }
}
