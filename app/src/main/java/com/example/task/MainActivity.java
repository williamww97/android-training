package com.example.task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.widget.SearchView;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.task.model.Account;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AccountAdapter.CallbackAdapter {
    LinearLayout linearLayout;
    RecyclerView recyclerView;
    AccountAdapter accountAdapter;
    CardView cv;
    ArrayList<Account> filteredList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cv = findViewById(R.id.acc_card);
        linearLayout = findViewById(R.id.main_activity);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //accountAdapter = new AccountAdapter(this, accountList(),this);
        accountAdapter = new AccountAdapter(this,this);
        accountAdapter.setDataAdapter(accountList());
        recyclerView.setAdapter(accountAdapter);
    }

    private ArrayList<Account> accountList(){
        ArrayList<Account> acclists = new ArrayList<>();

        Account acc = new Account("NICO","198623");
        acclists.add(acc);
        acc = new Account("ELVARETTA KATHINAWATI METTA","175176");
        acclists.add(acc);
        acc = new Account("IWAN PRATAMA","496746");
        acclists.add(acc);
        acc = new Account("DAVE ZACHARY RACHMAT","940505");
        acclists.add(acc);
        acc = new Account("SYARIEF NOOR PERMADI","542343");
        acclists.add(acc);
        acc = new Account("JOVAN REYNARDO","518915");
        acclists.add(acc);
        acc = new Account("ADITIA JORDI PUTRA","275451");
        acclists.add(acc);
        acc = new Account("MICHAEL JAMES TONG","904359");
        acclists.add(acc);
        acc = new Account("GIOVANI ANGGASTA TIJONO","899903");
        acclists.add(acc);
        acc = new Account("LULU GABRIELA KIMBERLY","895235");
        acclists.add(acc);
        acc = new Account("RINALDI WILOPO","150251");
        acclists.add(acc);
        acc = new Account("M AMIN NURHAKIKI","54754");
        acclists.add(acc);
        acc = new Account("RETNO DWISANTI","103026");
        acclists.add(acc);
        acc = new Account("VIEVIN EFENDY","312138");
        acclists.add(acc);
        acc = new Account("WILLIAM WIJAYA","775494");
        acclists.add(acc);
        acc = new Account("RISKA ADHITA","810293");
        acclists.add(acc);
        acc = new Account("CALVIN AUSTIN","717212");
        acclists.add(acc);
        acc = new Account("WILLIAM ANTONY","179393");
        acclists.add(acc);
        acc = new Account("RAHKMAH NABILA","605784");
        acclists.add(acc);
        acc = new Account("CHRISTOPHER HANS BAKTI TANTRA","339496");
        acclists.add(acc);



        return acclists;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filteredList = new ArrayList<>();
                for(int i = 0 ; i < accountList().size() ; i++){
                    if(accountList().get(i).getAccount().toLowerCase().contains(newText)){
                        filteredList.add(accountList().get(i));
                    }
                }
                if(filteredList.isEmpty()){
                    showSnackBar();
                }
                accountAdapter.setDataAdapter(filteredList);
                accountAdapter.notifyDataSetChanged();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }



    public void showSnackBar(Account acc){
            Snackbar snackbar = Snackbar.make(linearLayout ,acc.getName() + " " + acc.getAccount(),Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
    }

    public void showSnackBar(){
        Snackbar snackbar = Snackbar.make(linearLayout ,"Account does not exist",Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    public void onClick(Account acc) {
        showSnackBar(acc);
    }
}